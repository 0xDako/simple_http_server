#include <string.h>
#include <iostream>
#include <vector>
#include <locale>
#include <sstream>
#include <ctime>

#define _WINSOCK_DEPRECATED_NO_WARNINGS 
#include <winsock2.h>
#include <windows.h>
#pragma comment(lib,"ws2_32.lib")

/**
* �������� ������� ��������� ������������
* \param[in] argc ���������� ��������� ���������� ������
* \param[in] argv ��������� �� ��� ���������
*/
int main(int argc, char* argv[])
{

    if (argc <= 1) 
    {
        std::cerr << "Not enough arguments" << std::endl;
        return -1;
    }
	std::string url (argv[1]);
	std::string get_http = "GET / HTTP/1.1\r\nHost: " + url + "\r\nConnection: close\r\n\r\n";
    
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(2, 2), &wsaData) != 0) {    
        std::cerr << "WSAStartup failed.\n";
        return -1;
    }

    SOCKET Socket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (Socket < 0) 
    {
        std::cerr << "Socket init error\n";
        return -1;
    }

    struct hostent* host = gethostbyname(url.c_str());
        
    SOCKADDR_IN SockAddr;
    SockAddr.sin_port = htons(80);
    SockAddr.sin_family = AF_INET;
    SockAddr.sin_addr.s_addr = *((unsigned long*)host->h_addr);

    if (connect(Socket, (SOCKADDR*)(&SockAddr), sizeof(SockAddr)) != 0) {
        std::cerr << "Could not connect";
        return -1;
    }

    int good_try = 0;
    char buffer[100]{ 0 };
    time_t now = time(0);
    while (1) 
    {
        send(Socket, get_http.c_str(), (int)strlen(get_http.c_str()), 0);
        int len_recv_data = recv(Socket, buffer, 100, 0);
        if (len_recv_data != 0) good_try++;
        if(now != time(0)) 
        {
            now = time(0);
            std::cout << "Good connection per second:" << good_try << std::endl;
            good_try = 0;
        }
    }

    closesocket(Socket);
    WSACleanup();
}