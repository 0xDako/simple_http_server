#include <iostream>
#include <stdexcept>
#include <string>
#include <filesystem>
#include <fstream>


#ifdef WIN32
#include "server_lib/server_lib_win.h"
#elif __linux__ 
#include "server_lib/server_lib_unix.h"
#endif 

/**
*  \file
*  \brief Основной файл программы
*   Данный фаил содержит в себе функцию main c которой начинается выполнение программы
*/


/**
* Основная функция программы
* \param[in] argc Количество аргуметов коммандной строки
* \param[in] argv Указатели на эти аргументы
*/
int main(int argc, char* argv[])
{
    int port(80);
    
    std::string folder("");

    std::string config_file("");
    
    // Получим аргументы командной строки
    if (argc <= 1)
    {
        std::cerr << "Not enough arguments" << std::endl;
        return 1;
    }
    else
    {
        for (int i(0); i < argc; i++) 
        {
            std::string param(argv[i]);

            if (param.find("p=") == 0) 
            {
                param = param.erase(0, 2);
                port = std::stoi(param);
            }
            if (param.find("folder=") == 0) 
            {
                param = param.erase(0, 7);
                folder = std::filesystem::current_path().string()+"\\" + param;
            }
            if (param.find("config=") == 0) 
            {
                param = param.erase(0, 7);
                config_file = param;
            }

        }
    }
    // проверка правильности данных 
    if (config_file != "" && std::filesystem::exists(config_file))
    {
        std::ifstream  if_config;
        std::string line;
        if_config.open(config_file);
        
        while (std::getline(if_config, line)) 
        {
            if (line.find("p=") == 0)
            {
                line = line.erase(0, 2);
                port = std::stoi(line);
            }
            if (line.find("folder=") == 0)
            {
                line = line.erase(0, 7);
                folder = std::filesystem::current_path().string()+ "\\" + line;
            }
        }
        if_config.close();
        if (folder == "" || !std::filesystem::exists(folder))
        {
            std::cerr << "Server folder isn't valid or No argument \"folger\" or \"config\" " << std::endl;
            return 1;
        }
    }
    else 
    {
        if (folder == "" )
        {
            std::cerr << "Server folder isn't valid or No argument \"folger\" or \"config\" " << std::endl;
            return 1;
        }
    }


    std::cout << "Start server init" << std::endl;
    std::cout << "Port:" << port << std::endl;
    std::cout << "Server main folder:" <<folder<< std::endl;

    Server Server(port, folder);


    std::cout << "Server init success" <<std::endl;

    Server.StartListenerLoop();
    return 0;
}