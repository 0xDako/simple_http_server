#include "server_lib_win.h"

Server::Server(int port, std::string path)
{
	// подгружаем  Ws2_32.dll.
	int result = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (result != 0)
	{
		std::cerr <<  "Failed to WSAStartup:" << result;
		exit(-1);
	}

	struct addrinfo hints {};
	struct addrinfo* address = NULL;

	hints.ai_family = AF_INET;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_flags = AI_PASSIVE;

	// получаем информацию об адресе
	result = getaddrinfo("127.0.0.1", std::to_string(port).c_str(), &hints, &address);

	if (result != 0) {
		WSACleanup(); // выгрузка библиотеки Ws2_32.dll
		std::cerr << "getaddrinfo failed: " << result;
		exit(-1);
	}

	// Открываем сокет
	listen_socket = socket(address->ai_family, address->ai_socktype, address->ai_protocol);

	if (listen_socket == INVALID_SOCKET)
	{
		WSACleanup(); // выгрузка библиотеки ws2_32.dll
		freeaddrinfo(address); // освобождение памяти address
		std::cerr << "create socket error, WSAGetLastError: " << WSAGetLastError();
		exit(-1);
	}

	// Привязываем сокет к IP-адресу
	result = bind(listen_socket, address->ai_addr, (int)address->ai_addrlen);
	freeaddrinfo(address);

	if (result == SOCKET_ERROR) 
	{
		WSACleanup(); // выгрузка библиотеки ws2_32.dll
		closesocket(listen_socket); // Закрытие сокета
		std::cerr << "bind socket error, WSAGetLastError: " << WSAGetLastError();
		exit(-1);
	}

	// Инициализируем слушающий сокет
	if (listen(listen_socket, SOMAXCONN) == SOCKET_ERROR) 
	{
		WSACleanup(); // выгрузка библиотеки ws2_32.dll
		closesocket(listen_socket); // Закрытие сокета
		std::cerr << "init socket error, WSAGetLastError: " << WSAGetLastError();
		exit(-1);
	}
	// проветка path на корректнось
	if (path.back() != '\\') 
	{
		basic_path = path + "\\";
	}
	else 
	{
		basic_path = path ;
	}

}

Server::~Server()
{
	closesocket(listen_socket);
	WSACleanup();
}

void Server::StartListenerLoop()
{
	while (1) 
	{
		const int buffer_size = 1024;
		char buffer[buffer_size];
		SOCKET client_socket = INVALID_SOCKET;

		std::string link = "";
		// принимаем одно соединение

		client_socket = accept(listen_socket, NULL, NULL);
		if (client_socket == INVALID_SOCKET) {
			std::cerr << "Accept connection error, WSAGetLastError:" << std::to_string(WSAGetLastError());
		}

		// получаем header запроса в bufer
		int result = recv(client_socket, buffer, buffer_size, 0);

		if (result == SOCKET_ERROR)
		{
			closesocket(client_socket);
			std::cerr << "recv faliled:" + std::to_string(result);
		}
		else if (result == 0)
		{
			closesocket(client_socket);
			std::cerr << "Connection close by user" << std::endl;
		}
		else if (result > 0)
		{
			for (int i = 0; buffer[i] != '\n'; i++)
			{
				link += buffer[i];
			}
		}

		std::cout << "Received connection:" << link << std::endl;

		std::string request_type = link.substr(0, link.find(" "));
		//Проверяем корректность типа запроса 
		if (request_type != "GET") 
		{
			std::cerr << "Wrong request_type:" << request_type;
			result = send(client_socket, wrong_request_type_response_body, (int)strlen(wrong_request_type_response_body), 0);
			if (result == SOCKET_ERROR) {
				// произошла ошибка при отправле данных
				std::cerr << "send failed: " << WSAGetLastError() << "\n";
			}
			closesocket(client_socket);
			continue;
		}

		std::string file_name = link.substr(link.find(" ")+1,link.length());
		file_name = file_name.substr(1, file_name.find(" ")-1);
		//если клиент обращается в корень сервера, то перенаправляем его на страницу index.html
		if (file_name.empty())
		{
			file_name = "index.html";
		}
		//Проверяем существование запрашиваемого файла
		if (!std::filesystem::exists(basic_path+file_name)) 
		{
			result = send(client_socket, not_found_response_body,
				(int)strlen(not_found_response_body)	, 0);

			if (result == SOCKET_ERROR) {
				// произошла ошибка при отправле данных
				std::cerr << "send failed: " << WSAGetLastError() << "\n";
			}
			// Закрываем соединение к клиентом
			closesocket(client_socket);
			continue;
		}	

		std::stringstream response; // ответ клиенту
		std::stringstream response_body; // тело ответа

		std::string file_format = file_name.substr(file_name.find(".")+1, file_name.length());

		//формируем ответ пользователю(Меняем поле Content-Type в зависимости от типа запрашиваемого файла)
		if (file_format == "txt" || file_format == "html" || file_format == "htm" )
		{
			std::fstream file_stream(basic_path + file_name);
			response_body << file_stream.rdbuf();
			response << "HTTP/1.1 200 OK\r\n"
				<< "Version: HTTP/1.1\r\n"
				<< "Content-Type: text/html; charset=utf-8\r\n"
				<< "Content-Length: " << response_body.str().length()
				<< "\r\n\r\n"
				<< response_body.str();
			file_stream.close();
			result = send(client_socket, response.str().c_str(),
				(int)response.str().length(), 0);
		}
		else if (file_format == "css") 
		{
			std::fstream file_stream(basic_path + file_name);
			response_body << file_stream.rdbuf();
			response << "HTTP/1.1 200 OK\r\n"
				<< "Version: HTTP/1.1\r\n"
				<< "Content-Type: text/css; charset=utf-8\r\n"
				<< "Content-Length: " << response_body.str().length()
				<< "\r\n\r\n"
				<< response_body.str();
			file_stream.close();
			result = send(client_socket, response.str().c_str(),
				(int)response.str().length(), 0);
		}
		else if (file_format == "png")
		{
			std::ifstream file_stream(basic_path + file_name, std::ios::binary );
			
			std::vector<char> buffer(std::istreambuf_iterator<char>(file_stream), {});

			file_stream.close();

			response << "HTTP/1.1 200 OK\r\n"
				<< "Version: HTTP/1.1\r\n"
				<< "Content-Type: image/png;\r\n"
				<< "Content-Length: " << buffer.size()
				<< "\r\n\r\n";

			result = send(client_socket, response.str().c_str(), (int)response.str().length(), 0);
			result = send(client_socket, &buffer[0], (int)buffer.size(), 0);
			
		} 
		else if (file_format == "gif") 
		{
			std::ifstream file_stream(basic_path + file_name, std::ios::binary);

			std::vector<char> buffer(std::istreambuf_iterator<char>(file_stream), {});

			file_stream.close();

			response << "HTTP/1.1 200 OK\r\n"
				<< "Version: HTTP/1.1\r\n"
				<< "Content-Type: image/gif;\r\n"
				<< "Content-Length: " << buffer.size()
				<< "\r\n\r\n";

			result = send(client_socket, response.str().c_str(), (int)response.str().length(), 0);
			result = send(client_socket, &buffer[0], (int)buffer.size(), 0);
		}
		else if (file_format == "jpeg") 
		{
			std::ifstream file_stream(basic_path + file_name, std::ios::binary);

			std::vector<char> buffer(std::istreambuf_iterator<char>(file_stream), {});

			file_stream.close();

			response << "HTTP/1.1 200 OK\r\n"
				<< "Version: HTTP/1.1\r\n"
				<< "Content-Type: image/jpeg;\r\n"
				<< "Content-Length: " << buffer.size()
				<< "\r\n\r\n";

			result = send(client_socket, response.str().c_str(), (int)response.str().length(), 0);
			result = send(client_socket, &buffer[0], (int)buffer.size(), 0);
		}
		else if (file_format == "ico") 
		{
			std::ifstream file_stream(basic_path + file_name, std::ios::binary);
			std::vector<char> buffer(std::istreambuf_iterator<char>(file_stream), {});
			file_stream.close();
			response << "HTTP/1.1 200 OK\r\n"
				<< "Version: HTTP/1.1\r\n"
				<< "Content-Type: image/vnd.microsoft.icon;\r\n"
				<< "Content-Length: " << buffer.size()
				<< "\r\n\r\n";

			result = send(client_socket, response.str().c_str(), (int)response.str().length(), 0);
			result = send(client_socket, &buffer[0], (int)buffer.size(), 0);
		}

		if (result == -1) {
			// произошла ошибка при отправле данных
			std::cerr << "send failed: " << WSAGetLastError() << "\n";
		}
		// Закрываем соединение к клиентом
		closesocket(client_socket);
	}
}


