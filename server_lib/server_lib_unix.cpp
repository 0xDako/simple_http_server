#include "server_lib_unix.h"


Server::Server(int port, std::string path)
{

	// Открываем сокет
	listen_socket = socket(AF_INET, SOCK_STREAM, 0);
	if (listen_socket < 0 )
	{
		std::cerr << "create socket error, errno:" + std::to_string(errno);
		exit(-1);
	}
	
	struct sockaddr_in addr {};

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

	//Подключаемся слушающим сокетом к 127.0.0.1
	if (bind(listen_socket, (const struct sockaddr*)&addr, sizeof(addr)) <0)
	{	
		close(listen_socket);
		std::cerr << "bind socket error, errno:" + std::to_string(errno);
		exit(-1);
	}
	
	// Инициализируем слушающий сокет
	if (listen(listen_socket, 100) == -1)
	{
		close(listen_socket); // Закрытие сокета
		std::cerr << "init socket error, errno:" + std::to_string(errno);
		exit(-1);
	}

	// проветка path на корректнось
	if (path.back() != '/')
	{
		basic_path = path + "/";
	}
	else
	{
		basic_path = path;
	}

}

Server::~Server()
{
	close(listen_socket);
}

void Server::StartListenerLoop()
{
	while (1)
	{
		const int buffer_size = 1024;
		char buffer[buffer_size]{};
		int client_socket = -1;

		std::string link = "";
		// принимаем одно соединение

		client_socket = accept(listen_socket, NULL, NULL);
		if (client_socket < 0) {
			std::cerr << "Accept connection error, errno" << errno ;
		}

		// получаем данные запроса в bufer
		size_t result = recv(client_socket, buffer, buffer_size, 0);

		if (result < 0)
		{
			close(client_socket);
			std::cerr << "recv faliled:" + std::to_string(result);
		}
		else if (result == 0)
		{
			close(client_socket);
			std::cerr << "Connection close by user" << std::endl;
		}
		else if (result > 0)
		{
			for (int i = 0; buffer[i] != '\n'; i++)
			{
				link += buffer[i];
			}
		}

		std::cout << "Received connection:" << link << std::endl;

		std::string request_type = link.substr(0, link.find(" "));
		if (request_type != "GET")
		{
			std::cerr << "Wrong request_type:" << request_type;
			result = send(client_socket, wrong_request_type_response_body, strlen(wrong_request_type_response_body), 0);
			if (result < 0) {
				// произошла ошибка при отправле данных
				std::cerr << "send failed, errno:" << errno;
			}
			close(client_socket);
			continue;
		}

		std::string file_name = link.substr(link.find(" ") + 1, link.length());
		file_name = file_name.substr(1, file_name.find(" ") - 1);

		// если клиент обращается в корень сервера то перенаправляем его на index.html
		if (file_name.empty())
		{
			file_name = "index.html";
		}

		if (!std::filesystem::exists(basic_path + file_name))
		{
			result = send(client_socket, not_found_response_body,
				strlen(not_found_response_body), 0);

			if (result < 0) {
				// произошла ошибка при отправле данных
				std::cerr << "send failed, errno" << errno;
			}
			// Закрываем соединение к клиентом
			close(client_socket);
			continue;
		}


		std::stringstream response; // ответ
		std::stringstream response_body; // тело ответа

		std::string file_format = file_name.substr(file_name.find(".") + 1, file_name.length());

		//формируем ответ пользователю(Меняем поле Content-Type в зависимости от типа запрашиваемого файла)
		if (file_format == "txt" || file_format == "html" || file_format == "htm")
		{
			std::fstream file_stream(basic_path + file_name);
			response_body << file_stream.rdbuf();
			response << "HTTP/1.1 200 OK\r\n"
				<< "Version: HTTP/1.1\r\n"
				<< "Content-Type: text/html; charset=utf-8\r\n"
				<< "Content-Length: " << response_body.str().length()
				<< "\r\n\r\n"
				<< response_body.str();
			file_stream.close();
			result = send(client_socket, response.str().c_str(),
				response.str().length(), 0);
		}
		else if (file_format == "css")
		{
			std::fstream file_stream(basic_path + file_name);
			response_body << file_stream.rdbuf();
			response << "HTTP/1.1 200 OK\r\n"
				<< "Version: HTTP/1.1\r\n"
				<< "Content-Type: text/css; charset=utf-8\r\n"
				<< "Content-Length: " << response_body.str().length()
				<< "\r\n\r\n"
				<< response_body.str();
			file_stream.close();
			result = send(client_socket, response.str().c_str(),
				response.str().length(), 0);
		}
		else if (file_format == "png")
		{
			std::ifstream file_stream(basic_path + file_name, std::ios::binary);
			std::vector<char> buffer(std::istreambuf_iterator<char>(file_stream), {});
			file_stream.close();

			response << "HTTP/1.1 200 OK\r\n"
				<< "Version: HTTP/1.1\r\n"
				<< "Content-Type: image/png;\r\n"
				<< "Content-Length: " << buffer.size()
				<< "\r\n\r\n";

			result = send(client_socket, response.str().c_str(), response.str().length(), 0);
			result = send(client_socket, &buffer[0], buffer.size(), 0);

		}
		else if (file_format == "gif")
		{
			std::ifstream file_stream(basic_path + file_name, std::ios::binary);
			std::vector<char> buffer(std::istreambuf_iterator<char>(file_stream), {});
			file_stream.close();

			response << "HTTP/1.1 200 OK\r\n"
				<< "Version: HTTP/1.1\r\n"
				<< "Content-Type: image/gif;\r\n"
				<< "Content-Length: " << buffer.size()
				<< "\r\n\r\n";

			result = send(client_socket, response.str().c_str(), response.str().length(), 0);
			result = send(client_socket, &buffer[0], buffer.size(), 0);
		}
		else if (file_format == "jpeg")
		{
			std::ifstream file_stream(basic_path + file_name, std::ios::binary);
			std::vector<char> buffer(std::istreambuf_iterator<char>(file_stream), {});
			file_stream.close();

			response << "HTTP/1.1 200 OK\r\n"
				<< "Version: HTTP/1.1\r\n"
				<< "Content-Type: image/jpeg;\r\n"
				<< "Content-Length: " << buffer.size()
				<< "\r\n\r\n";

			result = send(client_socket, response.str().c_str(), response.str().length(), 0);
			result = send(client_socket, &buffer[0], buffer.size(), 0);
		}
		else if (file_format == "ico")
		{
			std::ifstream file_stream(basic_path + file_name, std::ios::binary);
			std::vector<char> buffer(std::istreambuf_iterator<char>(file_stream), {});
			file_stream.close();
			response << "HTTP/1.1 200 OK\r\n"
				<< "Version: HTTP/1.1\r\n"
				<< "Content-Type: image/vnd.microsoft.icon;\r\n"
				<< "Content-Length: " << buffer.size()
				<< "\r\n\r\n";

			result = send(client_socket, response.str().c_str(), response.str().length(), 0);
			result = send(client_socket, &buffer[0], buffer.size(), 0);
		}

		if (result == -1) {
			// произошла ошибка при отправле данных
			std::cerr << "send failed, errno" << errno;
		}
		// Закрываем соединение к клиентом
		close(client_socket);
	}
}


