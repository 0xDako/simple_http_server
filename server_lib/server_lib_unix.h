/*!
\file
\brief Заголовочный фаил библиотеки под Linux
*/
#include <iostream>
#include <string>
#include <sstream>
#include <filesystem>
#include <fstream>
#include <vector>
#include <iterator>
#include <cerrno>
#include <cstring>


#include <unistd.h>
#include <sys/types.h> 
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>

#include "error_messages.h"



/*!
	\brief Класс веб-сервера
	\author 0xDako
	\version 1.0
	\date Декабрь 2020 года

	Класс реализующий весь требуемый функционал для веб-сервера
*/
class Server
{
public:
/**
* \brief Конструкрой класса Server
* Открывает слушающий сокет на заданном порту и сохраняет его в поле listen_socket
*\param[in] port Порт на котором требуется открыть слушающий сокет
*\param[in] path Корневая деректория сервера
*/
	Server(int port, std::string path);
/**
* \brief Деструктор класса Server
*/
	~Server();
/**
* \brief Создает основной цикл северера
* Принимает и обрабатывает входящее соединение на сокете указанном в listen_socket
*/
	void StartListenerLoop();
protected:
private:
	struct sockaddr_in* address = NULL;
	int listen_socket = -1;
	std::string basic_path = "";
};

